// [SECTION]
db.fruits.aggregate([
    {$match: {$and: [{supplier: "Yellow Farms"}, {price: {$lt: 50}}]}},
    {$count: "itemsPriceLessThan50" }
])

//[SECTION]
db.fruits.aggregate([
    {$match: {price: {$lt: 30}}},
    {$count: "itemsPriceLessThan30" }
]) 

//[SECTION]
db.fruits.aggregate([
    {$match: {supplier: "Yellow Farms"}},
    {$group: {_id: "YellowFramAveragePrice", averagePrice: {$avg: "$price"}}}
])

//[SECTION]
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "RedFarmsMaxPrice", maxPrice: {$max: "$price"}}}
])


//[SECTION]
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "RedFarmsLowPrice", minPrice: {$min: "$price"}}}
])